<?php
 
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
 
return new class extends Migration
{
    public function up(): void
    {
        Schema::create('penjualans', function (Blueprint $table) {
            $table->id();
            $table->string('produk');
            $table->string('price');
            $table->string('jumlah');
            $table->string('nama_pembeli');
            $table->string('no_telp');
            $table->string('alamat');
            $table->timestamps();
        });
    }
    public function down(): void
    {
        Schema::dropIfExists('penjualans');
    }
};