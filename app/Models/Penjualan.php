<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Penjualan extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'produk',
        'price',
        'jumlah',
        'nama_pembeli',
        'no_telp',
        'alamat',
    ];
}
