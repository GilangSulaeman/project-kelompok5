<?php

namespace App\Http\Controllers;
use App\Models\Produksi;
use App\Models\Product;

use Illuminate\Http\Request;

class ProduksiController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        $produksi = Produksi::orderBy('created_at', 'DESC')->get();
  
        return view('produksis.index', compact('produksi'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $products = Product::orderBy('created_at', 'DESC')->get();
        return view('produksis.create', ['products' => $products]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
        Produksi::create($request->all());
 
        return redirect()->route('produksis')->with('success', 'Produksi added successfully');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
        $produksi= Produksi::findOrFail($id);
  
        return view('produksis.show', compact('produksi'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $produksi = Produksi::findOrFail($id);
        $products = Product::orderBy('created_at', 'DESC')->get();

        return view('produksis.edit', compact('produksi', 'products'));
    }

    public function update(Request $request, string $id)
    {
        $validatedData = $request->validate([
            'produk' => 'required',
            'jumlah' => 'required|numeric',
            'status' => 'required',
            'biaya_produksi' => 'required|numeric',
        ]);

        $produksi = Produksi::findOrFail($id);
        $produksi->update($validatedData);

        return redirect()->route('produksis')->with('success', 'Produksi updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
        $produksi = Produksi::findOrFail($id);
  
        $produksi->delete();
  
        return redirect()->route('produksis')->with('success', ' Data produksi deleted successfully');
    }
}
