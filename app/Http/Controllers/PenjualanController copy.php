<?php
  
namespace App\Http\Controllers;
  
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Penjualan;
 
class PenjualanController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $penjualan = Penjualan::orderBy('created_at', 'DESC')->get();
  
        return view('penjualans.index', compact('penjualan'));
    }
  
    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('penjualans.create');
    }
  
    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        Penjualan::create($request->all());
 
        return redirect()->route('penjualans')->with('success', 'Penjualan added successfully');
    }
  
    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $penjualan = Penjualan::findOrFail($id);
  
        return view('penjualans.show', compact('penjualan'));
    }
  
    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $penjualan = Penjualan::findOrFail($id);
  
        return view('penjualans.edit', compact('penjualan'));
    }
  
    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $penjualan = Penjualan::findOrFail($id);
  
        $penjualan->update($request->all());
  
        return redirect()->route('penjualans')->with('success', 'penjualan updated successfully');
    }
  
    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $penjualan = Penjualan::findOrFail($id);
  
        $penjualan->delete();
  
        return redirect()->route('penjualans')->with('success', 'Penjualan deleted successfully');
    }
}