<?php
  
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Models\Product;
 
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $product = Product::orderBy('created_at', 'DESC')->get();
        $productCount = $product->count();
        return view('products.index', compact('product', 'productCount'),[
            'active' => 'products',
        ]);

        
    }
  
    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('products.create');
    }
  
    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'price' => 'required|numeric',
            'product_code' => 'required', // Add other validation rules as needed
            'description' => 'required',
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048', // Adjust file types and size as needed
        ]);

        // Handle file upload
        if ($request->hasFile('photo')) {
            $photoPath = $request->file('photo')->store('product_photos', 'public');
        }

        // Create product with photo path
        $product = Product::create([
            'title' => $request->input('title'),
            'price' => $request->input('price'),
            'product_code' => $request->input('product_code'),
            'description' => $request->input('description'),
            'photo' => isset($photoPath) ? $photoPath : null,
        ]);

        return redirect()->route('products')->with('success', 'Product added successfully');
    }
  
    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $product = Product::findOrFail($id);
  
        return view('products.show', compact('product'));
    }
  
    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $product = Product::findOrFail($id);
  
        return view('products.edit', compact('product'));
    }
  
    /**
     * Update the specified resource in storage.
     */
    /**
 * Update the specified resource in storage.
 */
    public function update(Request $request, string $id)
    {
        $product = Product::findOrFail($id);

        // Validate input data
        $request->validate([
            'title' => 'required',
            'price' => 'required|numeric',
            'product_code' => 'required',
            'description' => 'required',
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048', // Adjust file types and size as needed
        ]);

        // Handle file upload for new photo
        if ($request->hasFile('photo')) {
            // Delete previous photo if exists
            if ($product->photo) {
                Storage::disk('public')->delete($product->photo);
            }

            // Upload new photo
            $photoPath = $request->file('photo')->store('product_photos', 'public');
        } else {
            // Keep existing photo path if no new photo is uploaded
            $photoPath = $product->photo;
        }

        // Update product with new data
        $product->update([
            'title' => $request->input('title'),
            'price' => $request->input('price'),
            'product_code' => $request->input('product_code'),
            'description' => $request->input('description'),
            'photo' => $photoPath,
        ]);

        return redirect()->route('products')->with('success', 'Product updated successfully');
    }

  
    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $product = Product::findOrFail($id);
  
        $product->delete();
  
        return redirect()->route('products')->with('success', 'product deleted successfully');
    }
}