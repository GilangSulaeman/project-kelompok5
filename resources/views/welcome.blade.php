<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />

    <title>Lojeans Home</title>
    <meta content="" name="description" />
    <meta content="" name="keywords" />

    <!-- Favicons -->
    <link href="{{ asset('assets/img/favicon1.png') }}" rel="icon" />
    <link href="assets/img/Logo Lojeans.png" rel="apple-touch-icon" />

    <!-- Google Fonts -->
    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Jost:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
        rel="stylesheet" />

    <!-- Vendor CSS Files -->
    <link href="{{ asset('assets/vendor/aos/aos.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/vendor/bootstrap-icons/bootstrap-icons.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/vendor/glightbox/css/glightbox.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/vendor/remixicon/remixicon.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/vendor/swiper/swiper-bundle.min.css') }}" rel="stylesheet" />

    <!-- Template Main CSS File -->
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" />
</head>

<body>
    <!-- ======= Header ======= -->
    <header id="header" class="fixed-top">
        <div class="container d-flex align-items-center">
            <h1 class="logo me-auto">
                <a href="index.html"><img src="assets/img/Artboard 14 copy.png" alt="logo" /></a>
            </h1>
            <!-- Navbar Start -->
            <nav id="navbar" class="navbar">
                <ul>
                    <li><a class="nav-link scrollto active" href="#hero">Home</a></li>
                    <li><a class="nav-link scrollto" href="#about">About</a></li>
                    <li><a class="nav-link scrollto" href="#portfolio">Product</a></li>
                    <li><a class="nav-link scrollto" href="#team">Team</a></li>
                    <li><a class="nav-link scrollto" href="#contact">Contact</a></li>
                    <li><a class="getstarted scrollto" href="{{ route('login') }}">Login</a></li>
                </ul>
                <i class="bi bi-list mobile-nav-toggle"></i>
            </nav>
            <!-- Navbar End -->
        </div>
    </header>
    <!-- End Header -->

    <!-- ======= Hero Section ======= -->
    <section id="hero" class="d-flex align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1"
                    data-aos="fade-up" data-aos-delay="200">
                    <h1>
                        With Our Patchwork Jeans Bags, Every Piece of Denim Holds a Story
                    </h1>
                    <h2>
                        Discover Sustainable Denim Artistry with Our Patchwork Jeans Bags
                    </h2>
                    <div class="d-flex justify-content-center justify-content-lg-start">
                        <a href="#about" class="btn-get-started scrollto">Get Started</a>
                        <a href="/Lojeans/assets/mp4/lojeans Vid.mp4" class="glightbox btn-watch-video"><i
                                class="bi bi-play-circle"></i><span>Watch Video</span></a>
                    </div>
                </div>
                <div class="col-lg-6 order-1 order-lg-2 hero-img" data-aos="zoom-in" data-aos-delay="200">
                    <img src="assets/img/img1.png" class="img-fluid animated" alt="" />
                </div>
            </div>
        </div>
    </section>
    <!-- End Hero -->

    <main id="main">
        <!-- ======= About Us Section ======= -->
        <section id="about" class="about">
            <div class="container" data-aos="fade-up">
                <div class="section-title">
                    <h2>About Us</h2>
                </div>

                <div class="row content">
                    <div class="col-lg-6">
                        <h3>
                            <strong>Visi</strong>
                        </h3>
                        <p>
                            Visi Lojeans adalah menjadi produsen terkemuka di industri totebag dari kain perca jeans,
                            diakui karena
                            inovasi, kualitas unggul, dan komitmen yang kuat terhadap keberlanjutan. Kami berambisi
                            untuk menciptakan
                            produk totebag yang tidak hanya mencerminkan gaya personal, tetapi juga mewakili perubahan
                            positif dalam
                            interaksi kita dengan lingkungan. Dengan fokus jangka panjang, kami berupaya menjadi
                            pemimpin dalam
                            menciptakan tren gaya yang bertanggung jawab, memberikan kekuatan kepada konsumen untuk
                            membuat pilihan
                            yang berkelanjutan tanpa mengorbankan gaya dan kualitas.
                        </p>
                    </div>
                    <div class="col-lg-6 pt-4 pt-lg-0">
                        <h3>
                            <Strong>Misi</Strong>
                        </h3>
                        <p>
                            1. Menghasilkan totebag berkualitas tinggi dari kain perca jeans yang tahan lama dan
                            fungsional.
                        </p>
                        <p>
                            2. Mengedepankan prinsip keberlanjutan dengan mendaur ulang kain jeans bekas untuk
                            mengurangi limbah
                            tekstil.
                        </p>
                        <p>
                            3. Terus mengembangkan desain unik dan menarik yang memenuhi kebutuhan dan gaya konsumen.
                        </p>
                        <a href="#" class="btn-learn-more">Learn More</a>
                    </div>
                </div>
            </div>
        </section>
        <!-- End About Us Section -->

        <!-- ======= Why Us Section ======= -->
        <section id="why-us" class="why-us section-bg">
            <div class="container-fluid" data-aos="fade-up">
                <div class="row">
                    <div
                        class="col-lg-7 d-flex flex-column justify-content-center align-items-stretch order-2 order-lg-1">
                        <div class="content">
                            <h3>
                                <strong>Kelebihan Lojeans</strong>
                            </h3>
                        </div>

                        <div class="accordion-list">
                            <ul>
                                <li>
                                    <a data-bs-toggle="collapse" class="collapse"
                                        data-bs-target="#accordion-list-1"><span>01</span> Tahan
                                        Lama <i class="bx bx-chevron-down icon-show"></i><i
                                            class="bx bx-chevron-up icon-close"></i></a>
                                    <div id="accordion-list-1" class="collapse show" data-bs-parent=".accordion-list">
                                        <p>
                                            Produk LoJeans dibuat dari kain perca jeans yang memiliki daya tahan tinggi,
                                            memastikan penggunaan
                                            jangka panjang tanpa khawatir akan kerusakan atau keausan.
                                        </p>
                                    </div>
                                </li>

                                <li>
                                    <a data-bs-toggle="collapse" data-bs-target="#accordion-list-2"
                                        class="collapsed"><span>02</span>
                                        Tampilan yang Menarik <i class="bx bx-chevron-down icon-show"></i><i
                                            class="bx bx-chevron-up icon-close"></i></a>
                                    <div id="accordion-list-2" class="collapse" data-bs-parent=".accordion-list">
                                        <p>
                                            Kain perca jeans memberikan sentuhan kasual yang stylish, sempurna untuk
                                            melengkapi berbagai gaya
                                            pakaian. Produk LoJeans tidak hanya praktis tetapi juga memberikan
                                            penampilan yang menarik dan
                                            trendi.
                                        </p>
                                    </div>
                                </li>

                                <li>
                                    <a data-bs-toggle="collapse" data-bs-target="#accordion-list-3"
                                        class="collapsed"><span>03</span>
                                        Ramah lingkungan <i class="bx bx-chevron-down icon-show"></i><i
                                            class="bx bx-chevron-up icon-close"></i></a>
                                    <div id="accordion-list-3" class="collapse" data-bs-parent=".accordion-list">
                                        <p>
                                            Penggunaan kain perca jeans oleh LoJeans bukan hanya tentang menciptakan
                                            produk yang berkualitas,
                                            tetapi juga mendukung praktik daur ulang dan penggunaan bahan yang ramah
                                            lingkungan. Ini adalah
                                            langkah kecil yang berkontribusi pada pelestarian lingkungan dan memberikan
                                            pilihan berkelanjutan
                                            kepada konsumen yang peduli lingkungan.
                                        </p>
                                    </div>
                                </li>

                                <li>
                                    <a data-bs-toggle="collapse" data-bs-target="#accordion-list-4"
                                        class="collapsed"><span>04</span>
                                        Fleksibilitas Penggunaan <i class="bx bx-chevron-down icon-show"></i><i
                                            class="bx bx-chevron-up icon-close"></i></a>
                                    <div id="accordion-list-4" class="collapse" data-bs-parent=".accordion-list">
                                        <p>
                                            Totebag LoJeans tidak hanya menjadi aksesori gaya, tetapi juga teman sejati
                                            untuk berbagai
                                            kesempatan. Dengan desain yang serbaguna, dapat digunakan untuk kegiatan
                                            sehari-hari, pertemuan
                                            formal, atau bahkan jalan - jalan akhir pekan.
                                        </p>
                                    </div>
                                </li>

                                <li>
                                    <a data-bs-toggle="collapse" data-bs-target="#accordion-list-5"
                                        class="collapsed"><span>05</span>
                                        Pengurangan Limbah Fashion <i class="bx bx-chevron-down icon-show"></i><i
                                            class="bx bx-chevron-up icon-close"></i></a>
                                    <div id="accordion-list-5" class="collapse" data-bs-parent=".accordion-list">
                                        <p>
                                            Dengan mendukung penggunaan kain perca jeans, LoJeans ikut berkontribusi
                                            pada pengurangan limbah
                                            fashion. Daur ulang bahan denim menjadi totebag adalah cara kreatif untuk
                                            mengurangi jejak
                                            lingkungan dan mengambil bagian dalam pergerakan menuju industri fashion
                                            yang lebih berkelanjutan.
                                        </p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-lg-5 align-items-stretch order-1 order-lg-2 img"
                        style="background-image: url('{{ asset('assets/img/Visi.jpg') }}" data-aos="zoom-in"
                        data-aos-delay="150">
                        &nbsp;
                    </div>
                </div>
            </div>
        </section>
        <!-- End Why Us Section -->

        <!-- ======= Product Section ======= -->
        <section id="portfolio" class="portfolio">
            <div class="container" data-aos="fade-up">
                <div class="section-title">
                    <h2>Product</h2>
                    <p>
                        Di sini, kami dengan bangga mempersembahkan beragam produk berkualitas tinggi yang diciptakan
                        oleh LoJeans.
                        Setiap item merupakan hasil dari dedikasi kami dalam menggunakan kain perca jeans yang tahan
                        lama,
                        memberikan tampilan yang menarik, serta mengusung nilai keberlanjutan. Temukan berbagai pilihan
                        produk
                        unggulan kami di bawah ini, yang tidak hanya memenuhi kebutuhan gaya Anda tetapi juga menjadi
                        langkah kecil
                        yang berarti dalam mendukung lingkungan.
                    </p>
                </div>

                <ul id="portfolio-flters" class="d-flex justify-content-center" data-aos="fade-up"
                    data-aos-delay="100">
                    <li data-filter="*" class="filter-active">All</li>
                    <li data-filter=".filter-app">Totebag</li>
                    <li data-filter=".filter-card">Pouch</li>
                    <li data-filter=".filter-web">Slingbag</li>
                </ul>

                <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">
                    <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                        <div class="portfolio-img">
                            <img src="assets/img/portfolio/produk-1.JPG" class="img-fluid" alt="" />
                        </div>
                        <div class="portfolio-info">
                            <h4>Chesstottebag</h4>
                            <p></p>
                            <a href="{{ asset('assets/img/portfolio/produk-1.JPG') }}"
                                data-gallery="portfolioGallery" class="portfolio-lightbox preview-link"
                                title="Chesstottebag"><i class="bx bx-plus"></i></a>
                            <a href="#" class="details-link" title="More Details"><i
                                    class="bx bx-link"></i></a>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 portfolio-item filter-web">
                        <div class="portfolio-img">
                            <img src="{{ asset('assets/img/portfolio/Produk-2.JPG') }}" class="img-fluid"
                                alt="" />
                        </div>
                        <div class="portfolio-info">
                            <h4>Slingbag</h4>
                            <p></p>
                            <a href="{{ asset('assets/img/portfolio/Produk-2.JPG') }}"
                                data-gallery="portfolioGallery" class="portfolio-lightbox preview-link"
                                title="Slingbag"><i class="bx bx-plus"></i></a>
                            <a href="#" class="details-link" title="More Details"><i
                                    class="bx bx-link"></i></a>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                        <div class="portfolio-img">
                            <img src="{{ asset('assets/img/portfolio/Produk-3.jpg') }}" class="img-fluid"
                                alt="" />
                        </div>
                        <div class="portfolio-info">
                            <h4>Chesstottebag</h4>
                            <p></p>
                            <a href="{{ asset('assets/img/portfolio/Produk-3.jpg') }}"
                                data-gallery="portfolioGallery" class="portfolio-lightbox preview-link"
                                title="Chesstotebag"><i class="bx bx-plus"></i></a>
                            <a href="#" class="details-link" title="More Details"><i
                                    class="bx bx-link"></i></a>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 portfolio-item filter-card">
                        <div class="portfolio-img">
                            <img src="{{ asset('assets/img/portfolio/Produk-4.jpeg') }}" class="img-fluid"
                                alt="" />
                        </div>
                        <div class="portfolio-info">
                            <h4>Pouch</h4>
                            <p></p>
                            <a href="{{ asset('assets/img/portfolio/Produk-4.jpeg') }}"
                                data-gallery="portfolioGallery" class="portfolio-lightbox preview-link"
                                title="Pouch"><i class="bx bx-plus"></i></a>
                            <a href="#" class="details-link" title="More Details"><i
                                    class="bx bx-link"></i></a>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 portfolio-item filter-web">
                        <div class="portfolio-img">
                            <img src="{{ asset('assets/img/portfolio/Produk-5.JPG') }}" class="img-fluid"
                                alt="" />
                        </div>
                        <div class="portfolio-info">
                            <h4>Slingbag</h4>
                            <p></p>
                            <a href="{{ asset('assets/img/portfolio/Produk-5.JPG') }}"
                                data-gallery="portfolioGallery" class="portfolio-lightbox preview-link"
                                title="Slingbag"><i class="bx bx-plus"></i></a>
                            <a href="#" class="details-link" title="More Details"><i
                                    class="bx bx-link"></i></a>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                        <div class="portfolio-img">
                            <img src="{{ asset('assets/img/portfolio/Produk-6.jpg') }}" class="img-fluid"
                                alt="" />
                        </div>
                        <div class="portfolio-info">
                            <h4>tottebag</h4>
                            <p></p>
                            <a href="{{ asset('assets/img/portfolio/Produk-6.jpg') }}"
                                data-gallery="portfolioGallery" class="portfolio-lightbox preview-link"
                                title="tottebag"><i class="bx bx-plus"></i></a>
                            <a href="#l" class="details-link" title="More Details"><i
                                    class="bx bx-link"></i></a>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 portfolio-item filter-card">
                        <div class="portfolio-img">
                            <img src="{{ asset('assets/img/portfolio/produk-7.jpeg') }}" class="img-fluid"
                                alt="" />
                        </div>
                        <div class="portfolio-info">
                            <h4>Pouch</h4>
                            <p></p>
                            <a href="{{ asset('assets/img/portfolio/produk-7.jpeg') }}"
                                data-gallery="portfolioGallery" class="portfolio-lightbox preview-link"
                                title="Pouch"><i class="bx bx-plus"></i></a>
                            <a href="#" class="details-link" title="More Details"><i
                                    class="bx bx-link"></i></a>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 portfolio-item filter-card">
                        <div class="portfolio-img">
                            <img src="{{ asset('assets/img/portfolio/Produk-8.jpeg') }}" class="img-fluid"
                                alt="" />
                        </div>
                        <div class="portfolio-info">
                            <h4>Pouch</h4>
                            <p></p>
                            <a href="{{ asset('assets/img/portfolio/Produk-8.jpeg') }}"
                                data-gallery="portfolioGallery" class="portfolio-lightbox preview-link"
                                title="Pouch"><i class="bx bx-plus"></i></a>
                            <a href="#" class="details-link" title="More Details"><i
                                    class="bx bx-link"></i></a>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 portfolio-item filter-web">
                        <div class="portfolio-img">
                            <img src="{{ asset('assets/img/portfolio/Produk-9.JPG') }}" class="img-fluid"
                                alt="" />
                        </div>
                        <div class="portfolio-info">
                            <h4>Slingbag</h4>
                            <p></p>
                            <a href="{{ asset('assets/img/portfolio/Produk-9.JPG') }}"
                                data-gallery="portfolioGallery" class="portfolio-lightbox preview-link"
                                title="Slingbag"><i class="bx bx-plus"></i></a>
                            <a href="#" class="details-link" title="More Details"><i
                                    class="bx bx-link"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Product Section -->

        <!-- ======= Team Section ======= -->
        <section id="team" class="team section-bg">
            <div class="container" data-aos="fade-up">
                <div class="section-title">
                    <h2>Our Teams</h2>
                    <p>
                        Kami adalah kelompok profesional yang menggabungkan energi, keahlian, dan semangat untuk meraih
                        keunggulan.
                        Dengan dedikasi kami terhadap kualitas dan inovasi, kami hadir untuk menciptakan solusi yang tak
                        terlupakan.
                    </p>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="member d-flex align-items-start" data-aos="zoom-in" data-aos-delay="100">
                            <div class="pic">
                                <img src="assets/img/team/" class="img-fluid" alt="" />
                            </div>
                            <div class="member-info">
                                <h4>Mivtakhul Jannah</h4>
                                <span>Product Manager</span>
                                <p>
                                    Saya Satriya Yoga Madhasatya, seorang Product Manager dengan semangat untuk
                                    mewujudkan visi dan
                                    menciptakan pengalaman produk yang tak terlupakan.
                                </p>
                                <div class="social">
                                    <a href=""><i class="ri-twitter-fill"></i></a>
                                    <a href=""><i class="ri-facebook-fill"></i></a>
                                    <a href=""><i class="ri-instagram-fill"></i></a>
                                    <a href=""> <i class="ri-linkedin-box-fill"></i> </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6 mt-4 mt-lg-0">
                        <div class="member d-flex align-items-start" data-aos="zoom-in" data-aos-delay="200">
                            <div class="pic">
                                <img src="assets/img/team/Foto-1.jpg" class="img-fluid" alt="" />
                            </div>
                            <div class="member-info">
                                <h4>Satriya yoga</h4>
                                <span>System Analyst</span>
                                <p>
                                    Saya Mivtakhul Jannah adalah seorang Analis Sistem yang memiliki keahlian luar biasa
                                    dalam memahami,
                                    merancang, dan mengimplementasikan solusi teknologi informasi yang efektif.
                                </p>
                                <div class="social">
                                    <a href=""><i class="ri-twitter-fill"></i></a>
                                    <a href=""><i class="ri-facebook-fill"></i></a>
                                    <a href=""><i class="ri-instagram-fill"></i></a>
                                    <a href=""> <i class="ri-linkedin-box-fill"></i> </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6 mt-4">
                        <div class="member d-flex align-items-start" data-aos="zoom-in" data-aos-delay="300">
                            <div class="pic">
                                <img src="assets/img/team/Foto-3.JPG" class="img-fluid" alt="" />
                            </div>
                            <div class="member-info">
                                <h4>Nafidanisa</h4>
                                <span>UI/UX Design</span>
                                <p>
                                    Saya Nafidanisa adalah seorang Desainer UI/UX yang berbakat dan kreatif dengan
                                    kemampuan luar biasa
                                    dalam
                                    merancang pengalaman pengguna yang memukau.
                                </p>
                                <div class="social">
                                    <a href=""><i class="ri-twitter-fill"></i></a>
                                    <a href=""><i class="ri-facebook-fill"></i></a>
                                    <a href=""><i class="ri-instagram-fill"></i></a>
                                    <a href=""> <i class="ri-linkedin-box-fill"></i> </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6 mt-4">
                        <div class="member d-flex align-items-start" data-aos="zoom-in" data-aos-delay="400">
                            <div class="pic">
                                <img src="assets/img/team/Foto-4.JPG" class="img-fluid" alt="" />
                            </div>
                            <div class="member-info">
                                <h4>Gilang Sulaeman</h4>
                                <span>Programmer</span>
                                <p>
                                    Saya Gilang Sulaeman adalah seorang Programmer yang handal, memiliki keahlian
                                    mendalam dalam
                                    mengembangkan
                                    solusi perangkat lunak yang efisien.
                                </p>
                                <div class="social">
                                    <a href=""><i class="ri-twitter-fill"></i></a>
                                    <a href=""><i class="ri-facebook-fill"></i></a>
                                    <a href=""><i class="ri-instagram-fill"></i></a>
                                    <a href=""> <i class="ri-linkedin-box-fill"></i> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Team Section -->

        <!-- ======= Contact Section ======= -->
        <section id="contact" class="contact">
            <div class="container" data-aos="fade-up">
                <div class="section-title">
                    <h2>Contact</h2>
                    <p>
                        Jika Anda memiliki pertanyaan atau memerlukan bantuan lebih lanjut, silakan hubungi kami melalui
                        informasi
                        kontak berikut:
                    </p>
                </div>

                <div class="row">
                    <div class="col-lg-5 d-flex align-items-stretch">
                        <div class="info">
                            <div class="address">
                                <i class="bi bi-geo-alt"></i>
                                <h4>Location:</h4>
                                <p>Jl. DI Panjaitan No.128, Karangreja, Purwokerto Kidul, Kec. Purwokerto Sel.,
                                    Kabupaten Banyumas, Jawa
                                    Tengah 53147</p>
                            </div>

                            <div class="email">
                                <i class="bi bi-envelope"></i>
                                <h4>Email:</h4>
                                <p>GilangSulaeman04@gmail.com</p>
                            </div>

                            <div class="phone">
                                <i class="bi bi-phone"></i>
                                <h4>Call:</h4>
                                <p>+62 895 1286 8829</p>
                            </div>

                            <iframe
                                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d63300.4313633599!2d109.20720274796447!3d-7.434574624060442!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e655ea49d9f9885%3A0x62be0b6159700ec9!2sInstitut%20Teknologi%20Telkom%20Purwokerto!5e0!3m2!1sid!2sid!4v1701793784526!5m2!1sid!2sid"
                                width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"
                                referrerpolicy="no-referrer-when-downgrade"></iframe>
                        </div>
                    </div>

                    <div class="col-lg-7 mt-5 mt-lg-0 d-flex align-items-stretch">
                        <form action="forms/contact.php" method="post" role="form" class="php-email-form">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="name">Your Name</label>
                                    <input type="text" name="name" class="form-control" id="name"
                                        required />
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="name">Your Email</label>
                                    <input type="email" class="form-control" name="email" id="email"
                                        required />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name">Subject</label>
                                <input type="text" class="form-control" name="subject" id="subject" required />
                            </div>
                            <div class="form-group">
                                <label for="name">Message</label>
                                <textarea class="form-control" name="message" rows="10" required></textarea>
                            </div>
                            <div class="my-3">
                                <div class="loading">Loading</div>
                                <div class="error-message"></div>
                                <div class="sent-message">
                                    Your message has been sent. Thank you!
                                </div>
                            </div>
                            <div class="text-center">
                                <button type="submit">Send Message</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Contact Section -->
    </main>
    <!-- End #main -->



    <div id="preloader"></div>
    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i
            class="bi bi-arrow-up-short"></i></a>

    <!-- Vendor JS Files -->
    <script src="assets/vendor/aos/aos.js"></script>
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
    <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
    <script src="assets/vendor/php-email-form/validate.js"></script>
    <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
    <script src="assets/vendor/waypoints/noframework.waypoints.js"></script>

    <!-- Template Main JS File -->
    <script src="assets/js/main.js"></script>
</body>

</html>
