@extends('layouts.app')
  
@section('title', 'Show Penjualan')
  
@section('contents')
    <h1 class="mb-0">Detail Penjualan</h1>
    <hr />
    <div class="row">
        <div class="col mb-3">
            <label class="form-label">Produk</label>
            <input type="text" name="produk" class="form-control" placeholder="produk" value="{{ $penjualan->produk}}" readonly>
        </div>
        <div class="col mb-3">
            <label class="form-label">Price</label>
            <input type="text" name="price" class="form-control" placeholder="price" value="{{ $penjualan->price }}" readonly>
        </div>
    </div>
    <div class="row">
        <div class="col mb-3">
            <label class="form-label">Jumlah</label>
            <input type="text" name="jumlah" class="form-control" placeholder="Jumlah" value="{{ $penjualan->jumlah }}" readonly>
        </div>
        <div class="col mb-3">
            <label class="form-label">Nama Pembeli</label>
            <input type="text" name="nama_pembeli" class="form-control" placeholder="Nama Pembeli" value="{{ $penjualan->nama_pembeli }}" readonly>
        </div>
    </div>
    <div class="row">
        <div class="col mb-3">
            <label class="form-label">No Telp</label>
            <input type="text" name="no_telp" class="form-control" placeholder="No Telp" value="{{ $penjualan->no_telp }}" readonly>
        </div>
        <div class="col mb-3">
            <label class="form-label">Alamat</label>
            <input type="text" name="alamat" class="form-control" placeholder="Alamat" value="{{ $penjualan->alamat }}" readonly>
        </div>
    </div>
    <div class="row">
        <div class="col mb-3">
            <label class="form-label">Created At</label>
            <input type="text" name="created_at" class="form-control" placeholder="Created At" value="{{ $penjualan->created_at }}" readonly>
        </div>
        <div class="col mb-3">
            <label class="form-label">Updated At</label>
            <input type="text" name="updated_at" class="form-control" placeholder="Updated At" value="{{ $penjualan->updated_at }}" readonly>
        </div>
    </div>
@endsection