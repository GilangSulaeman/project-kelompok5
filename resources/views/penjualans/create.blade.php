@extends('layouts.app')

@section('title', 'Create Penjualan')

@section('contents')
    <h1 class="mb-0">Add Penjualan</h1>
    <hr />
    <form action="{{ route('penjualans.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row mb-3">
            <div class="col">
                <select class="form-control" id="produk" name="produk" onchange="updatePrice()">
                    <option value="">Pilih Produk</option>
                    @foreach($products as $product)
                        <option value="{{ $product->title }}" data-price="{{ $product->price }}">{{ $product->title }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col">
                <!-- Menampilkan harga produk yang dipilih -->
                <p id="priceDisplay">Harga: </p>
                <!-- Input harga yang tersembunyi sebagai data pengiriman -->
                <input type="hidden" name="price" id="price" class="form-control">
            </div>
        </div>
        <div class="row mb-3">
            <div class="col">
                <input type="text" name="jumlah" class="form-control" placeholder="Jumlah">
            </div>
            <div class="col">
                <input type="text" name="nama_pembeli" class="form-control" placeholder="Nama Pembeli">
            </div>
        </div>
        <div class="row mb-3">
            <div class="col">
                <input type="text" name="no_telp" class="form-control" placeholder="No.Telp">
            </div>
            <div class="col">
                <input type="text" name="alamat" class="form-control" placeholder="Alamat">
            </div>
        </div>
        <div class="row">
            <div class="d-grid">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>

    <script>
        function updatePrice() {
            var select = document.getElementById('produk');
            var priceDisplay = document.getElementById('priceDisplay');
            var priceInput = document.getElementById('price');

            // Mendapatkan harga produk yang dipilih
            var selectedOption = select.options[select.selectedIndex];
            var selectedPrice = selectedOption.getAttribute('data-price');

            // Menampilkan harga pada elemen dan mengisi nilai input yang tersembunyi
            priceDisplay.innerText = 'Harga: ' + selectedPrice;
            priceInput.value = selectedPrice;
        }
    </script>
@endsection
