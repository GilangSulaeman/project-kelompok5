@extends('layouts.app')

@section('title', 'Edit Produksi')

@section('contents')
    <h1 class="mb-0">Edit Produksi</h1>
    <hr />
    @if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
    <form action="{{ route('produksis.update', $produksi->id) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="row mb-3">
            <div class="col">
                <select class="form-control" id="produk" name="produk" onchange="updatePrice()">
                    <option value="">Pilih Produk</option>
                    @foreach($products as $product)
                        <option value="{{ $product->title }}" data-price="{{ $product->price }}"{{ $produksi->produk == $product->title ? 'selected' : '' }}>{{ $product->title }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col">
                <input type="text" name="jumlah" class="form-control" placeholder="Jumlah" value="{{ $produksi->jumlah }}">
            </div>
        </div>
        <div class="row mb-3">
            <div class="col">
                <input type="text" name="status" class="form-control" placeholder="Status" value="{{ $produksi->status }}">
            </div>
            <div class="col">
                <input type="text" name="biaya_produksi" class="form-control" placeholder="Biaya Produksi" value="{{ $produksi->biaya_produksi }}">
            </div>
        </div>

        <div class="row">
            <div class="d-grid">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>

    <!-- JavaScript Function -->
    <script>
        function updatePrice() {
            // Add your logic here
            console.log('Price updated');
        }
    </script>
@endsection
