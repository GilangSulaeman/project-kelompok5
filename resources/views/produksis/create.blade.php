@extends('layouts.app')

@section('title', 'Create Produksi')

@section('contents')
    <h1 class="mb-0">Add Penjualan</h1>
    <hr />
    <form action="{{ route('produksis.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row mb-3">
            <div class="col">
                <select class="form-control" id="produk" name="produk" onchange="updatePrice()">
                    <option value="">Pilih Produk</option>
                    @foreach($products as $product)
                        <option value="{{ $product->title }}" data-price="{{ $product->price }}">{{ $product->title }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col">
                <input type="text" name="jumlah" class="form-control" placeholder="Jumlah">
            </div>
        </div>
        <div class="row mb-3">
            <div class="col">
                <input type="text" name="status" class="form-control" placeholder="Status">
            </div>
            <div class="col">
                <input type="text" name="biaya_produksi" class="form-control" placeholder="Biaya Produksi">
            </div>
        </div>
    
        <div class="row">
            <div class="d-grid">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
@endsection
