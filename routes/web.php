<?php
 
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\PenjualanController;
use App\Http\Controllers\ProduksiController;
 
Route::get('/', function () {
    return view('welcome');
});
 
Route::controller(AuthController::class)->group(function () {
    Route::get('register', 'register')->name('register');
    Route::post('register', 'registerSave')->name('register.save');
  
    Route::get('login', 'login')->name('login');
    Route::post('login', 'loginAction')->name('login.action');
  
    Route::get('logout', 'logout')->middleware('auth')->name('logout');
});
  
Route::middleware('auth')->group(function () {
    Route::get('dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
 
    Route::controller(ProductController::class)->prefix('products')->group(function () {
        Route::get('', 'index')->name('products');
        Route::get('create', 'create')->name('products.create');
        Route::post('store', 'store')->name('products.store');
        Route::get('show/{id}', 'show')->name('products.show');
        Route::get('edit/{id}', 'edit')->name('products.edit');
        Route::put('edit/{id}', 'update')->name('products.update');
        Route::delete('destroy/{id}', 'destroy')->name('products.destroy');
    });

    Route::controller(PenjualanController::class)->prefix('penjualans')->group(function () {
        Route::get('', 'index')->name('penjualans');
        Route::get('create', 'create')->name('penjualans.create');
        Route::post('store', 'store')->name('penjualans.store');
        Route::get('show/{id}', 'show')->name('penjualans.show');
        Route::get('edit/{id}', 'edit')->name('penjualans.edit');
        Route::put('edit/{id}', 'update')->name('penjualans.update');
        Route::delete('destroy/{id}', 'destroy')->name('penjualans.destroy');
    });

    Route::controller(ProduksiController::class)->prefix('produksis')->group(function () {
        Route::get('', 'index')->name('produksis');
        Route::get('create', 'create')->name('produksis.create');
        Route::post('store', 'store')->name('produksis.store');
        Route::get('show/{id}', 'show')->name('produksis.show');
        Route::get('edit/{id}', 'edit')->name('produksis.edit');
        Route::put('edit/{id}', 'update')->name('produksis.update');
        Route::delete('destroy/{id}', 'destroy')->name('produksis.destroy');
    });
 
    Route::get('/profile', [App\Http\Controllers\AuthController::class, 'profile'])->name('profile');
});